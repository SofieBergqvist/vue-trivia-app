import VueRouter from "vue-router";
import Home from "./components/Home.vue";
import Trivia from "./components/Trivia.vue";
import EndScreen from "./components/EndScreen.vue";
import NotFound from "./components/notFound/NotFound.vue";

const routes = [
  {
    path: "/",

    name: "Home",
    component: Home,
  },
  {
    path: "/trivia",
    name: "Trivia",
    component: Trivia,
  },
  {
    path: "/endscreen",
    name: "EndScreen",
    component: EndScreen,
    props: true,
  },
  {
    path: "*",
    component: NotFound,
  },
];

const router = new VueRouter({ routes });
export default router;
